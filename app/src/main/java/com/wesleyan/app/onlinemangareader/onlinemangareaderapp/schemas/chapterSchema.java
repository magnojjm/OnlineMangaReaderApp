package com.wesleyan.app.onlinemangareader.onlinemangareaderapp.schemas;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class chapterSchema implements Serializable {

    @SerializedName("ID")
    public String ID;
    @SerializedName("chapterTitle")
    public String chapterTitle;
    @SerializedName("pdfPath")
    public String pdfPath;
    @SerializedName("mangaID")
    public String mangaID;
}

