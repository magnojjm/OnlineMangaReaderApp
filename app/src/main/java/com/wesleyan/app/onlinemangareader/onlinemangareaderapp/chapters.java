package com.wesleyan.app.onlinemangareader.onlinemangareaderapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kosalgeek.android.json.JsonConverter;
import com.wesleyan.app.onlinemangareader.onlinemangareaderapp.schemas.chapterSchema;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class chapters extends AppCompatActivity  implements Serializable{
    View mView;
    AlertDialog.Builder mBuilder;
    EditText username,password;
    Button login,cancel;
    ListView listChapters;
    Bundle bundle;
    ArrayList<chapterSchema> listOfChapters;
    String mangaID,mangaName;
    ProgressDialog dialog12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapters);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bundle = getIntent().getExtras();
        if(getIntent().getExtras() != null){
            mangaID = bundle.getString("mangaID");
            mangaName = bundle.getString("mangaName");
        }

        getSupportActionBar().setTitle(mangaName);

        dialog12 = new ProgressDialog(chapters.this);
        mView = getLayoutInflater().inflate(R.layout.login, null);
        mBuilder = new AlertDialog.Builder(chapters.this);
        username = (EditText)mView.findViewById(R.id.username);
        password = (EditText)mView.findViewById(R.id.password);
        login = (Button)mView.findViewById(R.id.btnLogin);
        cancel = (Button)mView.findViewById(R.id.cancel);
        listChapters = (ListView)findViewById(R.id.chapterList);

        load();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Loging();
            }
        });




    }

    public void Loging(){

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.setCancelable(false);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(username.getText().toString().equals("admin") && password.getText().toString().equals("admin")){
                    Intent intent = new Intent(getApplicationContext(), AddChapters.class);
                    intent.putExtra("mangaID" , mangaID);
                    intent.putExtra("mangaName",mangaName);
                    startActivity(intent);
                }else{
                    Toast.makeText(chapters.this, "Incorrect Credentials", Toast.LENGTH_SHORT).show();
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent ref = new Intent(chapters.this,chapters.class);
                ref.putExtra("mangaID",mangaID);
                ref.putExtra("mangaName",mangaName);
                startActivity(ref);
                finish();
            }
        });
        dialog.show();
    }

    public void load(){
        String url = "https://onlinemangareader.000webhostapp.com/load_chapters.php";

        StringRequest showChapters = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.equals("No Results Found.")) {
                    notMatch();
                } else {
                    ArrayList<String> chapterName = new ArrayList<>();
                    listOfChapters = new JsonConverter<chapterSchema>().toArrayList(response, chapterSchema.class);
                    for (chapterSchema value : listOfChapters) {
                        chapterName.add(value.chapterTitle);
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.sizetext, chapterName);
                    listChapters.setAdapter(adapter);
                    listChapters.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            chapterSchema schemaChapter = listOfChapters.get(i);
                            Intent next = new Intent(chapters.this, reader.class);
                            next.putExtra("PDFPath", (Serializable) schemaChapter);
                            startActivity(next);
                        }
                    });

                }dialog12.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog12.dismiss();
                if (error instanceof TimeoutError){
                    Timeout();
                }else if (error instanceof NoConnectionError){
                    offlineMODE();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> postData = new HashMap<>();
                postData.put("mangaID",mangaID);
                return postData;
            }
        };
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(showChapters);
        dialog12.setTitle("Loading Chapters");
        dialog12.setCancelable(false);
        dialog12.setMessage("Please Wait");
        dialog12.show();
    }

    private void offlineMODE(){
        final AlertDialog alertDialog = new AlertDialog.Builder(chapters.this).create();
        alertDialog.setTitle("Offline");
        alertDialog.setMessage("Please Check your Internet Connection");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
    private void Timeout(){
        final AlertDialog alertDialog = new AlertDialog.Builder(chapters.this).create();
        alertDialog.setTitle("Connection Timeout");
        alertDialog.setMessage("Please Check your Internet Connection");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void notMatch(){
        final AlertDialog alertDialog = new AlertDialog.Builder(chapters.this).create();
        alertDialog.setTitle("No Result");
        alertDialog.setMessage("No Chapter in this Manga");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
