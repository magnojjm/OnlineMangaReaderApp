package com.wesleyan.app.onlinemangareader.onlinemangareaderapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    Handler handler;
    Runnable myRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new Handler();
        myRunnable = new Runnable() {
            @Override
            public void run() {
                Intent fin = new Intent(MainActivity.this,Home.class);
                startActivity(fin);
                finish();
            }
        };
        handler.postDelayed(myRunnable,3000);

    }


    @Override
    protected void onDestroy() {
        handler.removeCallbacks(myRunnable);
        super.onDestroy();
    }
}
