package com.wesleyan.app.onlinemangareader.onlinemangareaderapp.schemas;


import com.google.gson.annotations.SerializedName;

public class mangaSchema {

    @SerializedName("ID")
    public String ID;
    @SerializedName("title")
    public String title;
    @SerializedName("path")
    public String path;
}
