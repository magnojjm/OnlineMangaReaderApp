package com.wesleyan.app.onlinemangareader.onlinemangareaderapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.wesleyan.app.onlinemangareader.onlinemangareaderapp.R;
import com.wesleyan.app.onlinemangareader.onlinemangareaderapp.chapters;

import java.io.Serializable;
import java.util.ArrayList;


public class MangaAdapter extends RecyclerView.Adapter<MangaAdapter.PlaceViewHolder> implements Serializable {
    private Context context;
    private ArrayList<String> data_title;
    private ArrayList<String> data_images;
    private ArrayList<String> id;

    public MangaAdapter(Context context, ArrayList<String> data_title, ArrayList<String> data_images, ArrayList<String> id) {
        this.context = context;
        this.data_title = data_title;
        this.data_images = data_images;
        this.id = id;
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View placeView =  inflater.inflate(R.layout.item, parent, false);
        return new PlaceViewHolder(placeView);
    }

    @Override
    public void onBindViewHolder(final PlaceViewHolder holder, final int position) {

        final String title = data_title.get(position);
        final String images = data_images.get(position);
        final String ID = id.get(position);

        holder.tvID.setText(ID);
        holder.tvTitle.setText(title);
        Picasso.with(context).load(images).resize(130, 130).centerCrop().into(holder.ivCover);
        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next = new Intent(context, chapters.class);
                next.putExtra("mangaID",ID);
                next.putExtra("mangaName",title);
                context.startActivity(next);
            }
        });
        holder.ivCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next = new Intent(context, chapters.class);
                next.putExtra("mangaID",ID);
                next.putExtra("mangaName",title);
                context.startActivity(next);
            }
        });

    }

    @Override
    public int getItemCount() {
        return id.size();
    }


    public class PlaceViewHolder extends RecyclerView.ViewHolder {

        ImageView ivCover;
        TextView tvTitle,tvID;
        public PlaceViewHolder(View itemView) {
            super(itemView);
            ivCover = (ImageView) itemView.findViewById(R.id.ivCover);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvID = (TextView) itemView.findViewById(R.id.tv_id);
        }
    }
}
