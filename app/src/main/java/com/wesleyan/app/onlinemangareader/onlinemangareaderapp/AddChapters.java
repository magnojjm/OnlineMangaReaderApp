package com.wesleyan.app.onlinemangareader.onlinemangareaderapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import java.util.UUID;

public class AddChapters extends AppCompatActivity {

    Bundle bundle;
    String mangaID, mangaName;

    Button btnAdd, btnUpload;
    EditText chapterTitle ;
    Uri uri;
    public static final String PDF_UPLOAD_HTTP_URL = "https://onlinemangareader.000webhostapp.com/upload_chapters.php";

    public int PDF_REQ_CODE = 1;
    String titleHolder, pathHolder, pdfID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_chapters);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bundle = getIntent().getExtras();
        if(getIntent().getExtras() != null){
            mangaID = bundle.getString("mangaID");
            mangaName = bundle.getString("mangaName");
        }

        RequestRunTimePermission();

        btnUpload = (Button) findViewById(R.id.btnPDF);
        btnAdd = (Button) findViewById(R.id.btnSubmit1);
        chapterTitle = (EditText) findViewById(R.id.ctitle);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PdfUploadFunction();
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();

                intent.setType("application/pdf");

                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PDF_REQ_CODE);

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PDF_REQ_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            uri = data.getData();

            // After selecting the PDF set PDF is Selected text inside Button.
            btnUpload.setText("PDF is Selected");

        }
    }

    // PDF upload function starts from here.
    public void PdfUploadFunction() {

        titleHolder = chapterTitle.getText().toString().trim();

        pathHolder = FilePath.getPath(this, uri);
        if (pathHolder == null) {
            Toast.makeText(this, "Please move your PDF file to internal storage & try again.", Toast.LENGTH_LONG).show();
        }
        else {
            try {

                pdfID = UUID.randomUUID().toString();

                new MultipartUploadRequest(this, pdfID, PDF_UPLOAD_HTTP_URL)
                        .addFileToUpload(pathHolder, "pdf")
                        .addParameter("name", titleHolder)
                        .addParameter("mangaID",mangaID)
                        .setNotificationConfig(new UploadNotificationConfig())
                        .setMaxRetries(5)
                        .startUpload();
                Toast.makeText(AddChapters.this, "Uploading " + titleHolder +" to " + mangaName, Toast.LENGTH_SHORT).show();

            } catch (Exception exception) {

                Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    // Requesting run time permission method starts from here.
    public void RequestRunTimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(AddChapters.this, Manifest.permission.READ_EXTERNAL_STORAGE))
        {

            System.out.println("Done");

        } else {

            ActivityCompat.requestPermissions(AddChapters.this,new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] Result) {

        switch (RC) {

            case 1:

                if (Result.length > 0 && Result[0] == PackageManager.PERMISSION_GRANTED) {

                    System.out.println("Granted");

                } else {

                    System.out.println("Cancelled");

                }
                break;
        }
    }
}
