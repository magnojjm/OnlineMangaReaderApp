package com.wesleyan.app.onlinemangareader.onlinemangareaderapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class AddManga extends AppCompatActivity {
    ImageView ivProfile;
    Button btnAddPic,btnSubmit;
    EditText edTitle;
    GalleryPhoto galleryPhoto;
    final int GALLERY_REQUEST = 2200;
    String selectedPhoto;
    ProgressDialog dialog12;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_manga);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dialog12 = new ProgressDialog(AddManga.this);
        ivProfile = (ImageView)findViewById(R.id.ivPicture);
        btnAddPic = (Button)findViewById(R.id.btnAddPic);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        edTitle = (EditText)findViewById(R.id.title);
        galleryPhoto = new GalleryPhoto(AddManga.this);

        btnSubmit.setEnabled(false);

        btnAddPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edTitle.getText().toString().equals("")){
                    INC();
                }else {
                    try {
                        Bitmap bitmap = ImageLoader.init().from(selectedPhoto).requestSize(250, 250).getBitmap();
                        final String encodeIMG = ImageBase64.encode(bitmap);


                        String url = "https://onlinemangareader.000webhostapp.com/insert_manga.php";

                        StringRequest addManga = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                dialog12.dismiss();
                                if (response.equals("Try Again")) {
                                    Toast.makeText(AddManga.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(AddManga.this, "Manga Successfully Created", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(getApplicationContext(), Home.class);
                                    startActivity(intent);
                                    AddManga.this.finish();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                dialog12.dismiss();
                                if (error instanceof TimeoutError) {
                                    Timeout();
                                } else if (error instanceof NoConnectionError) {
                                    offlineMODE();
                                }
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> postData = new HashMap<>();
                                postData.put("title", edTitle.getText().toString());
                                postData.put("image", encodeIMG);
                                return postData;
                            }
                        };
                        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(addManga);
                        dialog12.setTitle("Inserting Manga");
                        dialog12.setCancelable(false);
                        dialog12.setMessage("Please Wait");
                        dialog12.show();
                    } catch (FileNotFoundException e) {
                        Toast.makeText(AddManga.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();

                    }
                    }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if(requestCode == GALLERY_REQUEST){
                galleryPhoto.setPhotoUri(data.getData());
                String photoPath = galleryPhoto.getPath();
                try {
                    Bitmap bitmap = ImageLoader.init().from(photoPath).requestSize(250, 250).getBitmap();
                    ivProfile.setImageBitmap(bitmap);
                    selectedPhoto = photoPath;
                    btnSubmit.setEnabled(true);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void offlineMODE(){
        final AlertDialog alertDialog = new AlertDialog.Builder(AddManga.this).create();
        alertDialog.setTitle("Offline");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Please Check your Internet Connection");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }
    private void Timeout(){
        final AlertDialog alertDialog = new AlertDialog.Builder(AddManga.this).create();
        alertDialog.setTitle("Connection Timeout");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Please Check your Internet Connection");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }
    private void INC(){
        final AlertDialog alertDialog = new AlertDialog.Builder(AddManga.this).create();
        alertDialog.setTitle("Incomplete");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Please Fill Up the Requirements");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }
}
