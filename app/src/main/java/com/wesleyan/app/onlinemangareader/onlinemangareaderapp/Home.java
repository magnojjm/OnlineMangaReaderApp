package com.wesleyan.app.onlinemangareader.onlinemangareaderapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kosalgeek.android.json.JsonConverter;
import com.wesleyan.app.onlinemangareader.onlinemangareaderapp.adapters.MangaAdapter;
import com.wesleyan.app.onlinemangareader.onlinemangareaderapp.schemas.mangaSchema;

import java.util.ArrayList;

public class Home extends AppCompatActivity {

    RecyclerView recyclerView;
    MangaAdapter adapter1;
    ProgressDialog dialog12;
    View mView;
    AlertDialog.Builder mBuilder;
    EditText username,password;
    Button login,cancel;
    ArrayList<String> id,title,picPath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        id = new ArrayList<String>();
        title = new ArrayList<String>();
        picPath = new ArrayList<String>();
        dialog12 = new ProgressDialog(Home.this);
        recyclerView = (RecyclerView)findViewById(R.id.rv_manga);
        mView = getLayoutInflater().inflate(R.layout.login, null);
        mBuilder = new AlertDialog.Builder(Home.this);
        username = (EditText)mView.findViewById(R.id.username);
        password = (EditText)mView.findViewById(R.id.password);
        login = (Button)mView.findViewById(R.id.btnLogin);
        cancel = (Button)mView.findViewById(R.id.cancel);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Loging();
            }
        });


        String url = "https://onlinemangareader.000webhostapp.com/load_manga.php";

        StringRequest showslams = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.equals("No Results Found.")) {
                    notMatch();
                } else {
                    ArrayList<mangaSchema> manga_info = new JsonConverter<mangaSchema>().toArrayList(response, mangaSchema.class);
                    for (mangaSchema value : manga_info) {
                        id.add(value.ID);
                        title.add(value.title);
                        picPath.add(value.path);
                    }

                    adapter1 = new MangaAdapter(Home.this,title,picPath,id);
                    recyclerView.setAdapter(adapter1);
                    recyclerView.setLayoutManager(new GridLayoutManager(Home.this, 2));

                }dialog12.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog12.dismiss();
                if (error instanceof TimeoutError){
                    Timeout();
                }else if (error instanceof NoConnectionError){
                    offlineMODE();
                }
            }
        });
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(showslams);
        dialog12.setTitle("Loading Manga");
        dialog12.setCancelable(false);
        dialog12.setMessage("Please Wait");
        dialog12.show();
    }

    public void Loging(){

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.setCancelable(false);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(username.getText().toString().equals("admin") && password.getText().toString().equals("admin")){
                    Intent intent = new Intent(getApplicationContext(), AddManga.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(Home.this, "Incorrect Credentials", Toast.LENGTH_SHORT).show();
                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                Intent ref = new Intent(Home.this,Home.class);
                startActivity(ref);
                finish();

            }
        });
        dialog.show();
    }
    private void offlineMODE(){
        final AlertDialog alertDialog = new AlertDialog.Builder(Home.this).create();
        alertDialog.setTitle("Offline");
        alertDialog.setMessage("Please Check your Internet Connection");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
    private void Timeout(){
        final AlertDialog alertDialog = new AlertDialog.Builder(Home.this).create();
        alertDialog.setTitle("Connection Timeout");
        alertDialog.setMessage("Please Check your Internet Connection");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
    private void notMatch(){
        final AlertDialog alertDialog = new AlertDialog.Builder(Home.this).create();
        alertDialog.setTitle("No Result");
        alertDialog.setMessage("No Manga in Database");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
